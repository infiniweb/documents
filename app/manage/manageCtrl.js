/**
 * Created by Santhosh on 21-Jul-15.
 */
manageModule.controller('ManageController', ['$scope','manageService','$modal','$timeout',function($scope, manageService, $modal, $timeout){
    $scope.timeNow = moment().format('DD-MMM-YYYY hh:mm a');
    $scope.documentList = [];
    manageService.getDocuments().then(function(result) {
        $scope.documentList = result.data;
        manageService.getComments().then(function(res){
            angular.forEach($scope.documentList, function(document){
                var findComment = _.findWhere(res.data, {DocumentId: document.Id});
                if(findComment){
                    document.Comments = findComment.Comments ;
                    document.CommentsId = findComment.Id;
                    document.Updated_Date = findComment.Updated_Date;
                }
            });
        })

    });

    $scope.fileUpload = function(document){
        $modal.open({
            animation: true,
            backdrop:'false',
            controller:'DocumentUploadController',
            templateUrl: 'manage/modals/documentUpload.tpl.html',
            size: 'md',
            resolve:{
                Document : function(){
                    return document;
                }
            }
        })
    };

    $scope.popover = {
        content: 'Sample',
        templateUrl: 'myPopoverTemplate.html',
        title: 'Title'
    };

    $scope.commentSaved = false;

    $scope.saveComments = function(document){
        var commentToSend = {};
        var method = document.CommentsId ? 'PUT' : 'POST';
        commentToSend.Id = document.CommentsId;
        commentToSend.DocumentId = document.Id;
        commentToSend.Comments = document.Comments;
        commentToSend.Updated_Date = new Date();

        manageService.saveComment(commentToSend, method).then(function(){
            $scope.commentSaved = true;
            $timeout(function(){
                $scope.commentSaved = false;
            }, 1000);
        }, function(){
            alert('Error, Please try again');
        })
    };

    $scope.filePreview = function(url){
        $modal.open({
            animation: true,
            backdrop:'false',
            templateUrl: 'manage/modals/documentPreview.html',
            size: 'md',
            resolve:{
                SourceUrl : function(){
                    return url;
                }
            }
        })
    };

}]);