/**
 * Created by Santhosh on 21-Jul-15.
 */
manageModule.service('manageService', ['$q','$http','AppSettings',function($q, $http, AppSettings){
    this.getDocuments = function(){
        return $http.get(AppSettings.BASE_URL+'Documents');
    };
    this.getComments = function(){
        return $http.get(AppSettings.BASE_URL+'Comments');
    };
    this.uploadDocument = function(data, method){
        var url = undefined;
        if(method == 'POST'){
            url = AppSettings.BASE_URL+'Documents';
        } else {
            url = AppSettings.BASE_URL+'Documents/'+data.Id;
        }
        return $http({
            method:method,
            url:url,
            data: data
        })
    };

    this.saveComment = function(data, method){
        var url = undefined;
        if(method == 'POST'){
            url = AppSettings.BASE_URL+'Comments';
        } else {
            url = AppSettings.BASE_URL+'Comments/'+data.Id;
        }
        return $http({
            method:method,
            url:url,
            data: data
        })
    };

}]);

manageModule.factory('DocumentFactory', function(){
    var DocumentFactory = {};
    DocumentFactory.DocumentObject = function(){
        this.Id= undefined;
        this.Type= undefined;
        this.Name= undefined;
        this.Availbility= true;
        this.RelativePath= undefined;
        this.UploadDate= undefined;
        this.UploadedBy= undefined;
    };
    DocumentFactory.createNewDocument = function(){
        return new DocumentFactory.DocumentObject();
    };
    return DocumentFactory
});