/**
 * Created by Santhosh on 21-Jul-15.
 */
manageModule.controller('DocumentUploadController', ['$scope','manageService','$modalInstance','Upload','Document','DocumentFactory','AppSettings','$timeout',
    function($scope, manageService, $modalInstance, Upload, Document, DocumentFactory, AppSettings, $timeout){

    $scope.documentType = 0;

    $scope.selectedFileName = undefined;
    $scope.documentTypes = [
        {id:0,name:'Select'},
        {id:1,name:'Plumbing'},
        {id:2,name:'Electrical'},
        {id:3,name:'Littering'}
    ];
    var documentToUpload = DocumentFactory.createNewDocument();

    $scope.close = function(){
        $modalInstance.close();
    };

    $scope.UploadSelectedFile = function (files, documentType, fileName) {
        var method = undefined;
        if(Document && Document.Id){
            documentToUpload.Id = Document.Id;
            method =  'PUT' ;
        } else {
            method =  'POST' ;
        }

        documentToUpload.Name = $scope.selectedFileName;
        documentToUpload.Type = _.findWhere($scope.documentTypes, {id:documentType}).name;
        documentToUpload.UploadDate = new Date();
        documentToUpload.Availbility = true;
        documentToUpload.UploadedBy = 'User';
        documentToUpload.RelativePath = 'UploadedFiles/'+documentToUpload.Type+'/'+files[0].name;

        Upload.upload({
            url: AppSettings.BASE_URL + 'Document/Upload?type='+documentToUpload.Type,
            file:files[0]
        }).progress(function (evt) {
            $scope.progressPercentage = parseInt(100.0 * evt.loaded / evt.total);

        }).success(function (data, status, headers, config) {
            manageService.uploadDocument(documentToUpload, method).then(function(result){
                $timeout(function(){
                    alert('Uploaded Successfully');
                    $scope.close();
                }, 1000);
            }, function(error){
                alert('Uploaded, But Not saved. Try again');
            });
        }).error(function (data, status, headers, config) {
            alert('Error, Try with a different file');
            $scope.progressPercentage = 0;
        });

    };

    $scope.$watch('files', function(newVal){
        if(newVal && newVal.length){
            var filename = newVal[0].name;
            $scope.selectedFileName = filename.split('.')[0];
        }

    })
}]);