'use strict';

// Declare app level module which depends on views, and components
angular.module('Infinijith', [
    // Third Party Modules
    'ui.router',
    'ui.bootstrap',
    'ngFileUpload',

    // App Modules
    'CommonModule',
    'ManageModule'
]).
config(['$urlRouterProvider','$stateProvider', function($urlRouterProvider, $stateProvider, $rootScope) {

        $urlRouterProvider.otherwise('manage');
        $stateProvider
            .state('manage', {
                url: '/manage',
                templateUrl: 'manage/manage.html',
                controller:'ManageController',
                title: 'Documentation Management'
        });


}]).
constant('AppSettings', {
        "BASE_URL":'http://localhost:50216/api/'
})
.run(['$rootScope',function($rootScope){
    // Need to dynamically configure the file download path in manage.html
    $rootScope.downloadLink = 'http://localhost:50216/';
}]);
